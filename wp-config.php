<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'farmacia_nova_prata');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1W3q6!@b1LoNA1HbQ)7XY@3d50n#2#Payk!xreHugTs9WEUyLZVd&xqPpopNEPeX');
define('SECURE_AUTH_KEY',  'XgIq92WKFqA7qsP5NLguG)pDDIAgvqDs6gVp5Oz3j1k*6Er9X0HWfo4R6tzfiXyq');
define('LOGGED_IN_KEY',    'RKG8cZCZH(0kSxLtZHn^veaSOuHTVaYwnbHjo1C(IullxElXEkg91Pc7A%Dxavu7');
define('NONCE_KEY',        'k6Sy3w6ALerDbMPhrS0jtT4^tVnTB0HWnqB^8qF2clIuFuGK0OY@6tsYiyL2^Knr');
define('AUTH_SALT',        'qeQ!t6TsQOH!Pr9*FRm3YqgUsGM46nvFSr4SXdj%^K&%@Icr*(ZiNna20H3H^f8U');
define('SECURE_AUTH_SALT', 'VdCwDKMy(ZW5uSzvq%r6i^XoYFCE2czdT1wZbzvK&%NqZDRizev2f)7I*(HgIfnR');
define('LOGGED_IN_SALT',   '0)H&OPrJ1OeqoS%LNnzq9mldTRv1Hets5SaOTiYUiXfVIcLY8p%5NH)dncpgToPL');
define('NONCE_SALT',       '*Keq6a2H9oo164%3DMlFMxjRtG!j!3UAMM0PTtTec6Ie%iGPzXHunq2vW7CPmJU0');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'Ff0uC0_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define( 'WP_ALLOW_MULTISITE', true );

define ('FS_METHOD', 'direct');
?>