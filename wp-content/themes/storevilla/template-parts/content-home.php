<?php global $post; ?>

<section id="dicas">
    <div class="title">
        <i class="material-icons md-24">assignment</i>
        <p>Mantenha-se Sempre Saudável!</p>
        <h4>Dicas de <span>Saúde</span></h4>
    </div>

    <div class="posts">
        <ul>
            <?php
            $args = array( 'numberposts' => 6, 'cat' => 1 );
            $myposts = get_posts( $args );
            foreach( $myposts as $post ) : setup_postdata($post); ?>
                <li>
                    <a href="<?php the_permalink(); ?>">
                        <?php if( has_post_thumbnail() ) : ?>
                            <img src="/wp-content/themes/storevilla/timthumb.php?src=<?php the_post_thumbnail_url(); ?>&w=370&h=410" />
                        <?php else : ?>
                            <img src=" <?php get_bloginfo( 'stylesheet_directory' ) . '/images/default.jpg' ?> "/>
                        <?php endif; ?>
                        <h4> <?php the_title(); ?> </h4>
                        <time datetime=<?php the_time('d/m/Y');?> > <?php the_time('d F, Y'); ?> </time>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>

    <a class="more" href="/category/dicas-de-saude">Ver mais<i class="material-icons">more</i> </a>
</section>

<section id="slider-body">
    <?php echo do_shortcode("[metaslider id=32]"); ?>
</section>

<section id="location">
    <div class="title">
        <h4>nossa <span>Localização</span></h4>
    </div>

    <div class="item">
        <div class="address">
            <ul>
                <li>
                    Matriz
                </li>
                <li>
                    <i class="material-icons">location_on</i>Av. Iguaçu, 742 - Centro
                </li>
                <li>
                    <i class="material-icons">phone</i><a href="tel:4635451422">46 3545.1422</a>
                </li>
                <li>
                    <i class="material-icons">phone_iphone</i><a href="https://api.whatsapp.com/send?phone=554691045959" target="_blank">Whatsapp 46 9 9104.5959</a>
                </li>
            </ul>
        </div>

        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3597.0424431999404!2d-53.343688126629274!3d-25.63671682100522!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94f1ab29a6baf359%3A0x88de53eb7e2e6494!2sAv.+Igua%C3%A7u%2C+742+-+Centro%2C+Nova+Prata+do+Igua%C3%A7u+-+PR%2C+85685-000!5e0!3m2!1spt-BR!2sbr!4v1513604714037" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>

    <div class="item">
        <div class="address">
            <ul>
                <li>
                    Filial
                </li>
                <li>
                    <i class="material-icons">location_on</i>Av. Iguaçu, 858 - Centro
                </li>
                <li>
                    <i class="material-icons">phone</i><a href="tel:4635451153">46 3545.1153</a>
                </li>
                <li>
                    <i class="material-icons">phone_iphone</i><a href="tel:46991067977">46 9 9106.7977</a>
                </li>
            </ul>
        </div>

        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3597.167982328777!2d-53.34781278485698!3d-25.632549747201324!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94f1ab278c6aaf21%3A0x7de16d505adfba2b!2sAv.+Igua%C3%A7u%2C+858+-+Centro%2C+Nova+Prata+do+Igua%C3%A7u+-+PR%2C+85685-000!5e0!3m2!1spt-BR!2sbr!4v1513603691948" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
</section>