<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Store_Villa
 */

get_header(); ?>

	<?php if ( is_home() || is_front_page() ) : ?>
		<?php echo do_shortcode("[metaslider id=40]"); ?>
	<?php endif; ?>

	<div class="store-container">

		<div class="content-area">

			<main id="main" class="site-main" role="main">

			<?php
				get_template_part( 'template-parts/content', 'home' );
			?>

			</main><!-- #main -->

		</div><!-- #primary -->

		<?php //get_sidebar('right'); ?>

	</div>

<?php get_footer();
