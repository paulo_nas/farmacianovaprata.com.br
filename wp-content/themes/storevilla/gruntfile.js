﻿module.exports = function( grunt ){

    var config = {        
        host: 'farmacianovaprata.dev'
    }

    //dependencies
    require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);    

    grunt.initConfig({        

        //WorkFolders
        folder : {
            js : 'assets/js/',
            assets :  'assets/',
            theme : ''
        },

        /* COMPILADOR LESS */
        less : {
            template : {
                files : {
                    '<%= folder.assets %>less/css/styles.css' : '<%= folder.assets %>less/main.less'
                }
            }
        },

        /* POST CSS AUTO-PREFIXER CSS */
        postcss : {
            options  : {
                processors : [
                    require('autoprefixer')({
                        remove: false,
                        browsers: ['last 4 versions']
                    })
                ]
            },
            template : {
                src: '<%= folder.assets %>less/css/*.css'
            }            
        },

        /* CSSMIN MINIFICA, COMBINA CSS */
        cssmin : {
            options : {
                keepSpecialComments: 0
            },
            template : {
                files : {
                    '<%= folder.theme %>styles.min.css': [
                        '<%= folder.assets %>less/css/styles.css'
                    ],
                }
            }    
        },

        /* UGLIFY MINIFICA */
        uglify : {
            options : {
                //preserveComments: 'false',
                screwIE8: true
            },
            template : {
                files : {
                    '<%= folder.js %>main.min.js': [
                        '<%= folder.js %>common.js',
                        '<%= folder.js %>customizer.js',
                        '<%= folder.js %>lightslider.js',
                        '<%= folder.js %>main.js',
                        '<%= folder.js %>modernizr.js',
                        '<%= folder.js %>navigation.js',
                        '<%= folder.js %>retina.js',
                        '<%= folder.js %>skip-link-focus-fix.js',
                        '<%= folder.js %>storevilla-init-admin.js'
                    ]                    
                }
            }
        },

        /* AUTO UPDATE - acompanha alterações nos arquivos */
        browserSync : {
            geral : {
                bsFiles : {
                    src: [
                        'js/**/*.js',
                        '<%= folder.assets %>css/**/*.css',
                        '<%= folder.theme %>'
                    ]
                },
                options : {
                    watchTask: true,
                    startPath: '/index.php',
                    proxy: config.host,
                    notify: false,
                    open: false
                }
            }
        },

        /* WATCH, VERIFICA ALTERAÇÕES NOS ARQUIVOS */
        watch : {
            options : {
                spawn: false,
            },
            template_css : {
                files: ['<%= folder.assets %>less/**/*.less'],
                tasks: ['less:template','postcss:template','cssmin:template']
            },     
            main_js : {
                files: [ '<%= folder.js %>**/*.js'],
                tasks: ['uglify:main']
            },
            gruntfile : {
                files: ['gruntfile.js']
            }
        }
        
    });

    /* TAREFA PADRÃO */
    grunt.registerTask('default', ['browserSync', 'watch']);

    /* TAREFA SÓ GRUNT */
    grunt.registerTask('grunt', ['watch']);

    /* TAREFA GERA TUDO */
    grunt.registerTask('init', ['less', 'postcss', 'cssmin']);

};